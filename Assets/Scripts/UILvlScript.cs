﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UILvlScript : MonoBehaviour
{

	public	Text	text;
	private Player player;
	// Use this for initialization
	void Start ()
	{
		player = GameObject.FindObjectOfType <Player> ();
	}
	
	// Update is called once per frame
	void Update ()
	{
		text.text = "Level\n" + player.level;
	}
}

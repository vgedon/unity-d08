﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIHpScript : MonoBehaviour
{
	public	Text	text;
	private Player player;
	private RectTransform hpBar;
	// Use this for initialization
	void Start ()
	{
		player = GameObject.FindObjectOfType <Player> ();
		hpBar = GetComponent <RectTransform> ();
	}

	// Update is called once per frame
	void LateUpdate ()
	{
		hpBar.localScale = new Vector3 (1, ((float)(player.HP) / (float)(player.maxHP)), 1);
		text.text = player.HP + " / " + player.maxHP;
	}
}

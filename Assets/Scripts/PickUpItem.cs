﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpItem : Item
{
	public	int heal = 10;
	private	Player player;
	private	float	spawnTime;

	// Use this for initialization
	void Start ()
	{
		spawnTime = Time.time;
		player = GameObject.FindObjectOfType <Player> ();
	}
	
	// Update is called once per frame
	void Update ()
	{
		float dist = Vector3.Distance (player.transform.position, transform.position);
		if (Time.time > spawnTime + 2 && dist <= player.pickUpRadius) {
			player.Heal (heal);
			Destroy (gameObject);
		}
	}
}

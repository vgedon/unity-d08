﻿using UnityEngine;
using UnityEngine.AI;
using System.Collections;

public class Player : ICharacter
{
	public	int	skillPoint = 0;
	public	int nextLevel = 0;
	public	int overallXP = 0;
	public	int pickUpRadius = 1;

	private	NavMeshAgent	meshAgent;
	private	Animator animator;

	// Use this for initialization
	void Start ()
	{
		initStats ();
		meshAgent = GetComponent <NavMeshAgent> ();
		animator = GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update ()
	{
		// Action
		if (Input.GetMouseButton (0)) {
			RaycastHit hit;
			Ray mouseRay = Camera.main.ScreenPointToRay (Input.mousePosition);
			// Debug.DrawLine (mouseRay.origin, mouseRay.origin + mouseRay.direction * 100f, Color.red, 3f);
			Physics.Raycast (mouseRay, out hit, Mathf.Infinity, Physics.AllLayers, QueryTriggerInteraction.Ignore);
			if (!target && hit.collider.tag == "Walkable" && hit.distance > meshAgent.stoppingDistance) {
				target = null;
				meshAgent.Stop ();
				transform.LookAt (hit.point);
				meshAgent.SetDestination (hit.point);
				meshAgent.Resume ();
				animator.SetBool ("Run", true);

				//Ennemy interaction
			} else if (!target && hit.collider.tag == "Ennemy") {
				target = hit.collider.gameObject.GetComponent <ICharacter> ();
				float	dist = Vector3.Distance (target.transform.position, transform.position);
				transform.LookAt (target.transform.position);
				if (dist < 1f) {
					meshAgent.Stop ();
					animator.SetBool ("Run", false);
					animator.SetTrigger ("Attack");
				} else {
					meshAgent.Stop ();
					meshAgent.SetDestination (target.transform.position);
					meshAgent.Resume ();
					animator.SetBool ("Run", true);
				}
			} else if (target && target.HP > 0) {

				float	dist = Vector3.Distance (target.transform.position, transform.position);
				transform.LookAt (target.transform.position);
				if (dist < 1f) {
					meshAgent.Stop ();
					animator.SetBool ("Run", false);
					animator.SetTrigger ("Attack");
				} else {
					meshAgent.Stop ();
					meshAgent.SetDestination (target.transform.position);
					meshAgent.Resume ();
					animator.SetBool ("Run", true);
				}
			}
		} else if (Input.GetMouseButtonUp (0)) {
			target = null;
		}
	}

	void LateUpdate ()
	{
		if (XP >= nextLevel) {
			level++;
			overallXP += nextLevel;
			XP = Mathf.Max (0, XP - nextLevel);
			nextLevel += nextLevel / 2;
//			Heal (maxHP);
		}
		if (meshAgent.remainingDistance == 0f) {
			animator.SetBool ("Run", false);
		}
	}

	public	override void Attack ()
	{
		if (HP > 0 && target) {
			target.TakeDamage (Random.Range (minDMG, maxDMG), AGI);
		}
	}

	public	override void Death ()
	{
		#if !UNITY_EDITOR
		UnityEngine.SceneManagement.SceneManager.LoadScene (UnityEngine.SceneManagement.SceneManager.GetActiveScene ().buildIndex);
		#endif
	}

	public	override void TakeDamage (float damage, int oponentAGI)
	{
		int chance = Random.Range (0, 100);
		if (chance < (75 + oponentAGI - AGI)) {
			HP -= damage * (1 - Armor / 200);
		}
		if (HP <= 0f) {
			animator.SetBool ("Run", false);
			animator.SetTrigger ("Death");
		}
	}

	public	override void Heal (int val)
	{
		HP = Mathf.Min (maxHP, HP + val);
	}

	private void initStats ()
	{
		if (level == 0) {
			level = 1;
		}
		float val;
		val = ((float)STR / 100f * 15f) * (level - 1);
		STR += (int)val;
		AGI += (int)val;
		CON += (int)val;
		maxHP = CON * 5;
		HP = maxHP;
		minDMG = STR / 2;
		maxDMG = minDMG + 4;
		nextLevel = maxHP;
	}
}

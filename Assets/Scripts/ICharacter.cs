﻿using UnityEngine;
using System.Collections;

public abstract class ICharacter : MonoBehaviour
{
	public int STR = 20;
	public int AGI = 20;
	public int CON = 20;
	public int XP = 0;
	public int Armor = 0;
	public float HP = 0;
	public int maxHP = 0;
	public int minDMG = 0;
	public int maxDMG = 0;
	public int level = 1;
	public int gold = 0;
	public ICharacter target;

	public abstract void Attack ();

	public abstract void TakeDamage (float damage, int oponentAGI);

	public abstract void Heal (int val);

	public abstract void Death ();
}
﻿using UnityEngine;
using UnityEngine.AI;
using System.Collections;
using System.Collections.Generic;

public class Ennemy : ICharacter
{
	private	NavMeshAgent	meshAgent;
	private	Animator	animator;
	public	List<Item>	lootItems;

	// Use this for initialization
	void Start ()
	{
		target = GameObject.FindObjectOfType <Player> ();
		initStats ();
		meshAgent = GetComponent <NavMeshAgent> ();
		animator = GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (HP > 0) {
			transform.LookAt (target.transform.position);
			float	dist = Vector3.Distance (target.transform.position, transform.position);
			if (dist < 1f) {
				animator.SetBool ("Run", false);
				meshAgent.Stop ();
				animator.SetTrigger ("Attack");
			} else if (dist <= 10f) {
				meshAgent.Resume ();
				animator.SetBool ("Run", true);
				meshAgent.SetDestination (target.transform.position);
			} else {
				animator.SetBool ("Run", false);
				meshAgent.Stop ();
			}
		}
	}

	void LateUpdate ()
	{
	}

	public	override void Attack ()
	{
		if (HP > 0 && target) {
			target.TakeDamage (Random.Range (minDMG, maxDMG), AGI);
		}
	}

	IEnumerator Hiding ()
	{
		yield return new WaitForSeconds (2f);
		for (int i = 0; i < 100; i++) {
			transform.Translate (Vector3.down * .005f);
			yield return new WaitForSeconds (.05f);
		}
		Destroy (gameObject);
	}

	public	override void Death ()
	{
		animator.Stop ();
		StartCoroutine (Hiding ());
		meshAgent.enabled = false;
		target.XP += XP;
		target.gold += gold;
		if (lootItems.Count > 0) {
			GameObject loot = lootItems [Random.Range (0, lootItems.Count)].gameObject;
			GameObject.Instantiate (loot);
			loot.transform.position = transform.position;
		}
	}

	public	override void TakeDamage (float damage, int oponentAGI)
	{
		int chance = Random.Range (0, 100);
		if (chance < (75 + oponentAGI - AGI)) {
			HP -= damage * (1 - Armor / 200);
		}
		if (HP <= 0f) {
			animator.SetBool ("Run", false);
			animator.SetTrigger ("Death");
		}
	}

	public	override void Heal (int val)
	{
		HP = Mathf.Min (maxHP, HP + val);
	}

	private void initStats ()
	{
		float val;
		level = target.level;
		val = (level - 1);
		STR += (int)val;
		AGI += (int)val;
		CON += (int)val;
		maxHP = CON * 2;
		HP = maxHP;
		minDMG = STR / 2;
		maxDMG = minDMG + 2;
		XP = maxHP;
		gold = Random.Range ((int)XP / 4, (int)XP / 2);
	}
}

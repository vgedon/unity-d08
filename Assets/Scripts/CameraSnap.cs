﻿using UnityEngine;
using System.Collections;

public class CameraSnap : MonoBehaviour {

	public GameObject	target;
	private Vector3		offset;
	private	Collider	coll;
	private bool		collide;

	// Use this for initialization
	void Start () {
		offset = transform.position - target.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		transform.position = target.transform.position + offset;
		RaycastHit[] hits = Physics.RaycastAll(transform.position, transform.forward);
		collide = false;
		foreach (RaycastHit hit in hits) {
			Debug.DrawLine(transform.position, hit.point, Color.red);
			if (hit.collider.tag == "HideObject"){
				collide = true;
				if (!coll){
					hit.collider.SendMessage("OnCollisionEnter");
					coll = hit.collider;
				}else if (hit.collider.Equals(coll)){
					coll.SendMessage("OnCollisionStay");
				}else if (!hit.collider.Equals(coll)){
					coll.SendMessage("OnCollisionExit");
					coll = hit.collider;
					hit.collider.SendMessage("OnCollisionEnter");
				}
				break;
			}
		}
		if (coll && !collide) {
			coll.SendMessage("OnCollisionExit");
			coll = null;
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIXpScript : MonoBehaviour
{
	public	Text	text;
	private Player player;
	private RectTransform xpBar;
	// Use this for initialization
	void Start ()
	{
		player = GameObject.FindObjectOfType <Player> ();
		xpBar = GetComponent <RectTransform> ();
	}
	
	// Update is called once per frame
	void LateUpdate ()
	{
		xpBar.localScale = new Vector3 (((float)(player.XP) / (float)(player.nextLevel)), 1, 1);
		text.text = (player.overallXP + player.XP) + " / " + (player.overallXP + player.nextLevel);
	}
}
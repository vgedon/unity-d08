﻿using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour
{
	public GameObject[] ennemyT;
	private GameObject clone;
	private float interval;
	public float max;
	public float min;

	// Use this for initialization
	void Start ()
	{
		interval = Random.Range (min, max);
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (interval <= 0 && !clone) {
			clone = Instantiate (ennemyT [Random.Range (0, ennemyT.Length)]);
			clone.transform.position = transform.position;
			interval = Random.Range (min, max);
		} else if (!clone) {
			interval -= Time.deltaTime;
		}
	}

	void OnDrawGizmos ()
	{
		Gizmos.DrawIcon (transform.position, "enemy.png", true);
	}
}

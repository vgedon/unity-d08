﻿using UnityEngine;
using System.Collections;

public class HideObject : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void Hide () {
		MeshRenderer[] tab = GetComponentsInChildren<MeshRenderer>();
		foreach (MeshRenderer item in tab) {
			item.enabled = false;
		}
		Debug.Log("hide: " + name);
	}
	
	public void Show () {
		MeshRenderer[] tab = GetComponentsInChildren<MeshRenderer>();
		foreach (MeshRenderer item in tab) {
			item.enabled = true;
		}
		Debug.Log("show: " + name);
	}
	
	void OnCollisionEnter(){
		Hide();
	}
	
	void OnCollisionStay(){
	}
	
	void OnCollisionExit(){
		Show();
	}
}
